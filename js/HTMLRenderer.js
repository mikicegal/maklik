/// <reference path="./interfaces.ts" />
var utilities;
(function (utilities) {
    var HTMLRenderer = (function () {
        function HTMLRenderer() {
            this.createTag = function (name, styleclass, id, attributes, innerText) {
                var render = "";
                render += "<" + name + " ";
                if (styleclass != null || typeof styleclass != 'undefined') {
                    render += "class='" + styleclass + "' ";
                }
                if (id != null || typeof id != 'undefined') {
                    render += "id='" + id + "' ";
                }
                if (attributes != null || typeof attributes != 'undefined') {
                    for (var _i = 0, attributes_1 = attributes; _i < attributes_1.length; _i++) {
                        var _a = attributes_1[_i], key = _a[0], value = _a[1];
                        render += key + "='" + value + "' ";
                    }
                }
                render += ">";
                if (innerText != null || typeof innerText != 'undefined') {
                    render += innerText + " ";
                }
                render += "</" + name + ">";
                return render;
            };
        }
        return HTMLRenderer;
    }());
    utilities.HTMLRenderer = HTMLRenderer;
})(utilities || (utilities = {}));
