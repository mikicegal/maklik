/// <reference path="./interfaces.ts" />
var utilities;
(function (utilities) {
    var HTMLRenderer = (function () {
        function HTMLRenderer() {
            var _this = this;
            this.createTag = function (name, styleclass, id, attributes, innerText) {
                var render = "";
                render += "<" + name + " ";
                if (styleclass != null || typeof styleclass != 'undefined') {
                    render += "class='" + styleclass + "' ";
                }
                if (id != null || typeof id != 'undefined') {
                    render += "id='" + id + "' ";
                }
                if (attributes != null || typeof attributes != 'undefined') {
                    for (var _i = 0, attributes_1 = attributes; _i < attributes_1.length; _i++) {
                        var obj = attributes_1[_i];
                        render += obj.key + "='" + obj.value + "' ";
                    }
                }
                render += ">";
                if (innerText != null || typeof innerText != 'undefined') {
                    render += innerText + " ";
                }
                render += "</" + name + ">";
                return render;
            };
            this.setBaseTemplate = function (template) {
                _this.baseTemplate = template;
            };
            this.fillTemplateWithAdapter = function (adapter, template, selector) {
                var regexp;
                for (var key in adapter) {
                    regexp = new RegExp("{{" + key + "}}", "g");
                    template.replace(regexp, adapter[key]);
                }
                return template;
            };
        }
        return HTMLRenderer;
    }());
    utilities.HTMLRenderer = HTMLRenderer;
    var DataSource = (function () {
        function DataSource(config) {
            var _this = this;
            this.fetch = function (config, callback) {
                _this.connection = new XMLHttpRequest();
                if (typeof config.user !== "undefined" && typeof config.password !== "undefined") {
                    _this.connection.open(config.method, config.uri, true, config.user, config.password);
                }
                else {
                    _this.connection.open(config.method, config.uri, true);
                }
                _this.connection.responseType = config.dataType;
                _this.connection.onreadystatechange = function () {
                    if (_this.connection.readyState === XMLHttpRequest.DONE && _this.connection.status === 200) {
                        _this.data = _this.connection.response;
                        callback(undefined, _this.data);
                    }
                    else if (_this.connection.readyState === XMLHttpRequest.DONE && _this.connection.status !== 200) {
                        callback({ status: _this.connection.status, statusText: "Not reaching the target correctly" }, undefined);
                    }
                };
                _this.connection.onerror = function () {
                    callback({ status: _this.connection.status, statusText: _this.connection.statusText }, undefined);
                };
                try {
                    if (typeof config.sendData !== 'undefined') {
                        _this.connection.send(config.sendData);
                    }
                    else {
                        _this.connection.send();
                    }
                }
                catch (e) {
                    error(404, "Not Found");
                }
            };
            this.sort = function (filters) {
                return [];
            };
            this.add = function (item) {
                _this.da;
            };
            this.commit = function () {
            };
            this.getJSONString = function () {
            };
            this.getObjectFromJSONString = function () {
            };
            if (typeof config != 'undefined') {
                this.fetch(config, function () { });
            }
        }
        return DataSource;
    }());
    utilities.DataSource = DataSource;
})(utilities || (utilities = {}));
/// <reference path="./utilities.ts" />
/// <reference path="./interfaces.ts" />
var Maklik = (function () {
    function Maklik() {
        var _this = this;
        this._HTMLRender = new utilities.HTMLRenderer();
        this._DataSource = new utilities.DataSource();
        this.hola = function () {
            var attribs = [];
            attribs.push({ key: "data-value", value: "test" });
            attribs.push({ key: "data-enabled", value: "true" });
            console.log(_this._HTMLRender.createTag("div", "prueba", "test", attribs, "Hola Mundo"));
            _this._DataSource.fetch({
                uri: "https://jsonplaceholder.typicode.com/posts",
                dataType: "json",
                method: "GET"
            }, function (err, response) {
                if (err) {
                    console.log(err);
                }
                else {
                    console.log(response);
                    console.log("exito");
                }
            });
        };
    }
    return Maklik;
}());
