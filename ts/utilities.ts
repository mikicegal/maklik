/// <reference path="./interfaces.ts" />
namespace utilities {

    export class HTMLRenderer implements Interfaces.IHTMLRenderer {
        private baseTemplate: string;

        public createTag = (name: string, styleclass?: string | null, id?: string | null, attributes?: Array<Interfaces.Map> | null, innerText?: string | null) => {
            let render = "";
            render += "<" + name + " ";
            if (styleclass != null || typeof styleclass != 'undefined') {
                render += "class='" + styleclass + "' ";
            }
            if (id != null || typeof id != 'undefined') {
                render += "id='" + id + "' ";
            }
            if (attributes != null || typeof attributes != 'undefined') {
                for (var obj of attributes) {
                    render += obj.key + "='" + obj.value + "' ";
                }
            }
            render += ">";
            if (innerText != null || typeof innerText != 'undefined') {
                render += innerText + " ";
            }
            render += "</" + name + ">";
            return render;
        }

        public setBaseTemplate = (template: string) => {
            this.baseTemplate = template;
        }

        public fillTemplateWithAdapter = (adapter: Object, template: string, selector: string) => {
            let regexp: RegExp;
            for (let key in adapter) {
                regexp = new RegExp("{{" + key + "}}", "g");
                template.replace(regexp, adapter[key]);
            }
            return template;
        }
    }

    export class DataSource implements Interfaces.IDataSource {
        private request: string;
        private connection: XMLHttpRequest;
        private data: Array<Object>;
        constructor(config?: Interfaces.IDataSourceConfig) {
            if (typeof config != 'undefined') {
                this.fetch(config, () => { })
            }
        }
        public fetch = (config: Interfaces.IDataSourceConfig, callback: (err: { status: number, statusText: string }, response: Object) => void) => {
            this.connection = new XMLHttpRequest();
            if (typeof config.user !== "undefined" && typeof config.password !== "undefined") {
                this.connection.open(config.method, config.uri, true, config.user, config.password);
            } else {
                this.connection.open(config.method, config.uri, true);
            }
            this.connection.responseType = config.dataType;
            this.connection.onreadystatechange = () => {
                if (this.connection.readyState === XMLHttpRequest.DONE && this.connection.status === 200) {
                    this.data = this.connection.response;
                    callback(undefined, this.data);
                } else if (this.connection.readyState === XMLHttpRequest.DONE && this.connection.status !== 200) {
                    callback({ status: this.connection.status, statusText: "Not reaching the target correctly" }, undefined);
                }
            }
            this.connection.onerror = () => {
                callback({ status: this.connection.status, statusText: this.connection.statusText }, undefined);
            };
            try {
                if (typeof config.sendData !== 'undefined') {
                    this.connection.send(config.sendData);
                } else {
                    this.connection.send();
                }
            } catch (e) {
                error(404, "Not Found")
            }
        }
        public sort = (filters: Array<Interfaces.Map>) => {
            return [];
        }

        public add = (item: Object) => {
            this.da
        }
        public commit = () => {

        }
        public getJSONString = () => {

        }
        public getObjectFromJSONString = () => {

        }

    }

}
