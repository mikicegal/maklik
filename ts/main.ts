/// <reference path="./utilities.ts" />
/// <reference path="./interfaces.ts" />
class Maklik {
    private _HTMLRender: utilities.HTMLRenderer = new utilities.HTMLRenderer();
    private _DataSource: utilities.DataSource = new utilities.DataSource();
    public hola = () => {
        var attribs: Array<Interfaces.Map> = [];
        attribs.push({ key: "data-value", value: "test" });
        attribs.push({ key: "data-enabled", value: "true" });
        console.log(this._HTMLRender.createTag("div", "prueba", "test", attribs, "Hola Mundo"));
        this._DataSource.fetch(
            {
                uri: "https://jsonplaceholder.typicode.com/posts",
                dataType: "json",
                method: "GET"
            }
            , function (err, response) {
                if (err) {
                    console.log(err);
                } else {
                    console.log(response);
                    console.log("exito")
                }
            });
    }
}
