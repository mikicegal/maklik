namespace Interfaces {
    export interface Map {
        key: string,
        value: string
    }
    export interface IHTMLRenderer {
        createTag: (name: string, styleclass?: string | null, id?: string | null, attributes?: Array<Map> | null, innerText?: string | null) => string,
        setBaseTemplate: (template: string) => void,
        fillTemplateWithAdapter: (adapter: Object, template: string, selector: string) => string
    }
    export interface IDataSource {
        sort: (filters: Array<Map>) => Array<any>,
        add: (item: Object) => any,
        commit: (uri: string, error: () => any, success: () => any) => any,
        getJSONString: (uri: string, error: () => any, success: (string: string) => any) => void,
        getObjectFromJSONString: (uri: string, error: () => any, success: (object: Object) => any) => void,
        fetch:(config: Interfaces.IDataSourceConfig, callback:(err:{status:number,statusText:string}, response:Object)=>void ) => void
    }
    export interface IDataSourceConfig {
        uri:string,
        method:string,
        dataType:string,
        user?:string,
        password?:string,
        sendData?: ArrayBuffer | ArrayBufferView | Blob | FormData | Document | Object
    }

}
